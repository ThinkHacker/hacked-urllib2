from cookielib import CookieJar
import _httplib
import _urllib2
VERSION = (0, 0, 1)

httplib = _httplib.__dict__
urllib2 = _urllib2.urllib2
